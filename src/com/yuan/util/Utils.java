package com.yuan.util;

import javax.swing.ImageIcon;

public class Utils {
	public static ImageIcon getImageIcon(String file) {
		return new ImageIcon(ImageIcon.class.getResource("res/"+file));
	}
}
