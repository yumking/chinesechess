package com.yuan.piece;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import com.yuan.chinesechess.ChessMain;
import com.yuan.chinesechess.Map;

public class Guard extends PieceBase {
	public Guard(int type, int x, int y) {
		super(type, x, y);
		// TODO Auto-generated constructor stub
	}
	
	public String getImagePath() {
		if (type == 1) {     		 //红色方
			return imagePath+"RA.GIF";
		} else {				     //黑色方
			return imagePath+"BA.GIF";
		}
	}
	
	public String getSelectedImagePath() {
		if (type == 1) {     		 	 //红色方
			return imagePath+"RAS.GIF";
		} else {					     //黑色方
			return imagePath+"BAS.GIF";
		}
	}

	
	public boolean move (Map map, int mx, int my) {
		boolean flag = false;
		int type = map.getValue(x, y).getType();
		if (type == 1) {
			if ((mx >=3 && mx <= 5) && (my >= 7)) {
				if (mx - x == 1 || x - mx == 1) {
					if (my - y == 1 || y - my == 1) {
						map.setValue(map.getValue(x, y), mx, my);
						map.setValue(null, x, y);
						setX(mx);
						setY(my);
						flag = true;
					}
				}
			}
		} else if (type == 0) {
			if ((mx >=3 && mx <= 5) && (my <= 2)) {
				if (mx - x == 1 || x - mx == 1) {
					if (my - y == 1 || y - my == 1) {
						map.setValue(map.getValue(x, y), mx, my);
						map.setValue(null, x, y);
						setX(mx);
						setY(my);
						flag = true;
					}
				}
			}
		}
		return flag;
	}
	
	public void draw(Graphics g, String path) {
		g.drawImage(new ImageIcon(path).getImage(), x * ChessMain.LEN, y * ChessMain.LEN, null);
	}
}
