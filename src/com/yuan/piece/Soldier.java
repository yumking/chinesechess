package com.yuan.piece;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import com.yuan.chinesechess.ChessMain;
import com.yuan.chinesechess.Map;

public class Soldier extends PieceBase {
	public Soldier(int type, int x, int y) {
		super(type, x, y);
		// TODO Auto-generated constructor stub
	}
	
	public String getImagePath() {
		if (type == 1) {     		 //��ɫ��
			return imagePath+"RP.GIF";
		} else {				     //��ɫ��
			return imagePath+"BP.GIF";
		}
	}
	
	public String getSelectedImagePath() {
		if (type == 1) {     		 	 //��ɫ��
			return imagePath+"RPS.GIF";
		} else {					     //��ɫ��
			return imagePath+"BPS.GIF";
		}
	}

	
	public boolean move (Map map, int mx, int my) {
		boolean flag = false;
		int type = map.getValue(x, y).getType();
		if (type == 1) {		//�췽
			if (my > 4) {		//δ���ֻ��ǰ��
				if (y - my == 1 && x == mx) {
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			} else {
				if ((y - my == 1 && x == mx) || (x - mx == 1 && y == my) || (mx - x == 1 && y == my)) {
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			}
		} else if (type == 0) {			// �ڷ�
			if (my < 5) {		//δ���ֻ��ǰ��
				if (my - y == 1 && x == mx) {
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			} else {
				if ((my - y == 1 && x == mx) || (x - mx == 1 && y == my) || (mx - x == 1&& y == my)) {
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			}
		}
		return flag;
	}
	
	public void draw(Graphics g, String path) {
		g.drawImage(new ImageIcon(path).getImage(), x * ChessMain.LEN, y * ChessMain.LEN, null);
	}
}
