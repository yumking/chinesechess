package com.yuan.piece;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import com.yuan.chinesechess.ChessMain;
import com.yuan.chinesechess.Map;

public class Elephant extends PieceBase {
	public Elephant(int type, int x, int y) {
		super(type, x, y);
		// TODO Auto-generated constructor stub
	}
	
	public String getImagePath() {
		if (type == 1) {     		 //红色方
			return imagePath+"RB.GIF";
		} else {				     //黑色方
			return imagePath+"BB.GIF";
		}
	}
	
	public String getSelectedImagePath() {
		if (type == 1) {     		 	 //红色方
			return imagePath+"RBS.GIF";
		} else {					     //黑色方
			return imagePath+"BBS.GIF";
		}
	}
	
	public boolean move (Map map, int mx, int my) {
		boolean flag = false;
		if ((y <= 4 && my <= 4) || (y >= 5 && my >= 5)) {
			if (mx - x == 2 && my - y == 2) {
				if (map.getValue(x+1, y+1) == null) {
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			} else if (mx - x == 2 && y - my == 2) {
				if (map.getValue(x+1, y-1) == null) {
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			} else if (x - mx == 2 && my - y == 2) {
				if (map.getValue(x-1, y+1) == null) {
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			} else if (x - mx == 2 && y - my == 2) {
				if (map.getValue(x-1, y-1) == null) {
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			}  
		}		
		return flag;
	}
	
	public void draw(Graphics g, String path) {
		g.drawImage(new ImageIcon(path).getImage(), x * ChessMain.LEN, y * ChessMain.LEN, null);
	}
}
