package com.yuan.piece;

import java.awt.Graphics;
import java.io.Serializable;

import com.yuan.chinesechess.Map;

public class PieceBase implements Cloneable,Serializable {
	protected int type = 1;     // 1��ʾ�췽�� 0��ʾ�ڷ�
	protected int x = 0;      	// x���
	protected int y = 0;		// y���
	protected String imagePath = "res/DELICATE/";
	public PieceBase(int type, int x, int y) {
		this.type = type;
		this.x = x;
		this.y = y;
	}
	
	public void setImagePath(String path) {
		this.imagePath = path;
	}
	
	public boolean move(Map map, int mx, int my) {
		return false;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public String getImagePath() {
		return null;
	}
	
	public String getSelectedImagePath() {
		return null;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void draw(Graphics g, String path) {
		
	}
	
	public Object clone() throws CloneNotSupportedException  
    {  
        return super.clone();  
    } 

}
