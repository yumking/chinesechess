package com.yuan.piece;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import com.yuan.chinesechess.ChessMain;
import com.yuan.chinesechess.Map;

public class Veicle extends PieceBase {

	public Veicle(int type, int x, int y) {
		super(type, x, y);
		// TODO Auto-generated constructor stub
	}
	
	public String getImagePath() {
		if (type == 1) {     		 //红色方
			return imagePath+"RR.GIF";
		} else {				     //黑色方
			return imagePath+"BR.GIF";
		}
	}
	
	public String getSelectedImagePath() {
		if (type == 1) {     		 	 //红色方
			return imagePath+"RRS.GIF";
		} else {					     //黑色方
			return imagePath+"BRS.GIF";
		}
	}

	
	public boolean move (Map map, int mx, int my) {
		boolean flag = false;
		if (mx == x && my != y) {
			int startY = 0;
			int endY = 0;
			if (y > my) {
				startY = my;
				endY = y;
			} else {
				startY = y;
				endY = my;
			}
			int moveY = 0;    
			for (int i = startY+1; i < endY; i++) {
				if (map.getValue(x, i) != null) {
					moveY = 1;
					break;
				}
				moveY = 0;
			}
			if (moveY == 0) {   	//move
				map.setValue(map.getValue(x, y), mx, my);
				map.setNull(x, y);
				setY(my);
				flag = true;
			}
			
		} else if (my == y && mx != x) {
			int startX = 0;
			int endX = 0;
			if (x > mx) {
				startX = mx;
				endX = x;
			} else {
				startX = x;
				endX = mx;
			}
			int moveX = 0;    
			for (int i = startX+1; i < endX; i++) {
				if (map.getValue(i, y) != null) {
					moveX = 1;
					break;
				}
				moveX = 0;
			}
			if (moveX == 0) {   	//move				
				map.setValue(map.getValue(x, y), mx, my);
				map.setNull(x, y);
				setX(mx);
				flag = true;
			}
			
		}
		return flag;
	}
	
	public void draw(Graphics g, String path) {
		g.drawImage(new ImageIcon(path).getImage(), x * ChessMain.LEN, y * ChessMain.LEN, null);
	}

}
