package com.yuan.piece;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import com.yuan.chinesechess.ChessMain;
import com.yuan.chinesechess.Map;

public class Horse extends PieceBase {

	public Horse(int type, int x, int y) {
		super(type, x, y);
		// TODO Auto-generated constructor stub
	}
	
	public String getImagePath() {
		if (type == 1) {     		 //红色方
			return imagePath+"RN.GIF";
		} else {				     //黑色方
			return imagePath+"BN.GIF";
		}
	}
	
	public String getSelectedImagePath() {
		if (type == 1) {     		 	 //红色方
			return imagePath+"RNS.GIF";
		} else {					     //黑色方
			return imagePath+"BNS.GIF";
		}
	}

	
	public boolean move (Map map, int mx, int my) {
		boolean flag = false;
		if (mx - x == 1 || x - mx == 1) {
			if (my - y == 2) {
				if (map.getValue(x, y + 1) == null) {   //move
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			} else if (y - my == 2) {
				if (map.getValue(x, y - 1) == null) {   //move
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			}
		} else if (my - y == 1 || y - my == 1) {
			if (mx - x == 2) {
				if (map.getValue(x + 1, y) == null) {   //move
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			} else if (x - mx == 2) {
				if (map.getValue(x - 1, y) == null) {   //move
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			}
		}
		return flag;
	}
	
	public void draw(Graphics g, String path) {
		g.drawImage(new ImageIcon(path).getImage(), x * ChessMain.LEN, y * ChessMain.LEN, null);
	}

}
