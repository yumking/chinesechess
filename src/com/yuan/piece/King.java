package com.yuan.piece;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import com.yuan.chinesechess.ChessMain;
import com.yuan.chinesechess.Map;

public class King extends PieceBase {
	public King(int type, int x, int y) {
		super(type, x, y);
		// TODO Auto-generated constructor stub
	}
	
	public String getImagePath() {
		if (type == 1) {     		 //红色方
			return imagePath+"RK.GIF";
		} else {				     //黑色方
			return imagePath+"BK.GIF";
		}
	}
	
	public String getSelectedImagePath() {
		if (type == 1) {     		 	 //红色方
			return imagePath+"RKS.GIF";
		} else {					     //黑色方
			return imagePath+"BKS.GIF";
		}
	}

	
	public boolean move (Map map, int mx, int my) {
		boolean flag = false;
		int type = map.getValue(x, y).getType();
		if (type == 1) {				//红方
			if ((mx >=3 && mx <= 5) && (my >= 7)) {       //行动范围在九宫格内
				if ((mx - x == 1 && my == y) || (x - mx == 1 && my == y) || (y - my == 1 && mx == x) || (my - y == 1 && mx == x)) {
					for (int i = my; i >= 0; i--) {
						if (map.getValue(mx, i) != null && !(map.getValue(mx, i) instanceof King) ) {
							break;
						}
						if ( map.getValue(mx, i) != null && map.getValue(mx, i) instanceof King ) {
							return false;
						}
					}
					map.setValue(map.getValue(x, y), mx, my);
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			}
		} else if (type == 0) {          //黑方
			if ((mx >=3 && mx <= 5) && (my <= 2)) {
				if ((mx - x == 1 && my == y) || (x - mx == 1 && my == y) || (y - my == 1 && mx == x) || (my - y == 1 && mx == x)) {
					for (int i = my; i < Map.y; i++) {
						if (map.getValue(mx, i) != null && !(map.getValue(mx, i) instanceof King) ) {
							break;
						}
						if ( map.getValue(mx, i) != null && map.getValue(mx, i) instanceof King ) {
							return false;
						}
					}
					map.setValue(map.getValue(x, y), mx, my);
					
					map.setValue(null, x, y);
					setX(mx);
					setY(my);
					flag = true;
				}
			}
		}
		return flag;
	}
	
	public void draw(Graphics g, String path) {
		g.drawImage(new ImageIcon(path).getImage(), x * ChessMain.LEN, y * ChessMain.LEN, null);
	}
}
