package com.yuan.chinesechess;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;

public class ChessPanel extends JPanel {
	private Image img;
	public ChessPanel() {
		setPreferredSize(ChessMain.size);
	}
	
	public void setImg(Image img) {
		this.img = img;
	}
	
	public void paintComponent(Graphics g) {
		if (img != null) {
			g.drawImage(img, 0, 0, null);
		}
	}
}
