package com.yuan.chinesechess;

import java.io.Serializable;

import com.yuan.piece.PieceBase;

public class Map implements Serializable{
	public static final int x = 9;
	public static final int y = 10;
	private PieceBase map[][];
	public Map () {
		map = new PieceBase[x][y];
	}
	
	public PieceBase[][] getMap() {
		return map;
	}
	
	public void setValue(PieceBase piece, int x, int y) {
		map[x][y] = piece;
	}
	
	public PieceBase getValue(int x, int y) {
		return map[x][y];
	}
	
	public void setMap(PieceBase map[][]) {
		this.map = map;
	} 
	
	public void setNull(int x, int y) {
		map[x][y] = null;
	}
}
