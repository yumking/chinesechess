package com.yuan.web;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
        private ServerSocket ss;
        public Socket socket;
        private int port = 3301;
        public Server() throws IOException{
                ss = new ServerSocket(port);
                System.out.println("waiting for connect....");

                Thread thread = new Thread(new Runnable(){
                        
                        @Override
                        public void run() {
                                // TODO Auto-generated method stub
                                try {
                                        socket = ss.accept();
                                } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                }
                                System.out.println(socket.getInetAddress().toString()+" is comming...");
                                new WebMain(socket);
                        }                       
                });
                thread.start();
        }
}
