package com.yuan.web;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
public class ChatPanel extends JPanel {
        private JTextField msgTyping;
        private JTextArea text;
        private JButton msgSend;
        private WebMain wm;
        private Socket socket;
        public ChatPanel(WebMain wm) {
                this.wm = wm;
                socket = wm.socket;
                
                setSize(200,450);
                text = new JTextArea();
                text.setLineWrap(true);
                text.setColumns(15);
                text.setRows(20);
                text.setEnabled(false);
                msgTyping = new JTextField();
                msgTyping.setColumns(10);
                msgSend = new JButton("send");
                setLayout(new BorderLayout());
                add(text,BorderLayout.CENTER);
                JPanel p = new JPanel();
                p.setLayout(new FlowLayout());
                p.add(msgTyping);
                p.add(msgSend);
                add(p,BorderLayout.SOUTH);
                receiveMsg();
                
                msgSend.addActionListener(new ActionListener(){
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                // TODO Auto-generated method stub
                                try {
                                        if (msgTyping.getText() != null) {
                                                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                                                pw.println(msgTyping.getText());
                                                pw.flush();
                                                text.append(msgTyping.getText()+"\r\n");
                                                msgTyping.setText("");
                                        }                                      
                                } catch (IOException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                }           
                        }                        
                });
        }
        
        public void receiveMsg() {
                new Thread(new Runnable() {
                        @Override
                        public void run() {
                                // TODO Auto-generated method stub
                                BufferedReader br;
                                try {
                                        br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                                        String str = br.readLine();
                                        while (str != null) {
                                                text.append(str+"\r\n");                                               
                                                str = br.readLine();
                                        }
                                } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                }                              
                        }                       
                }).start();
        }
}