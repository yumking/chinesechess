package com.yuan.web;

import java.io.IOException;
import java.net.Socket;

public class Client {
        private String ip;
        private int port = 3301;
        private Socket socket;
        public Client(String ip, int port) {
                this.ip = ip;
                this.port = port;
                try {
                        socket = new Socket(ip, port);
                        new WebMain(socket);
                } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
        }

}
